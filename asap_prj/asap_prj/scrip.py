# -*- coding: utf-8 -*-

from __future__ import print_function
from recsys.algorithm.factorize import SVD
from recsys.datamodel.item import Item
from recsys.datamodel.user import User
import constants as constants
import mysql.connector
import os.path
from scipy.spatial import distance
import operator

def make_query(query_text, cursor, cnx):
    query = (query_text)
    data = []
    try:
        cursor.execute(query)
        for row in cursor:
            for col in row:
                data.append(col)
        cnx.commit()
    except Exception as e:
        pass
    return data


cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
cursor_main_db = cnx_main_db.cursor()
cursor_main_db.execute("use " + constants.MainDB)

query_text = "SELECT id, name FROM menu_items;"

id_name = make_query(query_text, cursor_main_db, cnx_main_db)

f = open('id_name_item', 'w')

index = 0
while index < len(id_name):
    print(str(id_name[index]) + " ---> " + id_name[index + 1].encode('utf8'), file=f)
    index += 2

