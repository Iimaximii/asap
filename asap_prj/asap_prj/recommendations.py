from recsys.algorithm.factorize import SVD
from recsys.datamodel.item import Item
from recsys.datamodel.user import User
import constants as constants
import mysql.connector
import os.path
from scipy.spatial import distance
import operator


def make_query(query_text, cursor, cnx):
    query = (query_text)
    data = []
    try:
        cursor.execute(query)
        for row in cursor:
            for col in row:
                data.append(col)
        cnx.commit()
    except Exception as e:
        print e
    return data


def update_matr_count_venues():
    svd_data = open(SvdData, 'a')
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)

    query_text = "SELECT id FROM users;"
    user_ids = make_query(query_text, cursor_main_db, cnx_main_db)
    for user_id in list(set(user_ids)):
        query_text = "SELECT venue_id FROM orders where user_id=" + "\'" + str(user_id) + "\'" + ";"
        venue_ids = make_query(query_text, cursor_main_db, cnx_main_db)
        for venue_id in list(set(venue_ids)):
            rating = venue_ids.count(venue_id)
            svd.add_tuple(rating, user_id, venue_id)
    svd.compute(k=100, min_values=0, pre_normalize=None, mean_center=True, post_normalize=True, savefile=SvdData)
    return


def get_matr_count_weighted_tags():
    svd = SVD()
    if not os.path.isfile(constants.SvdData + "weighted_tag.zip"):
#       svd_data = open(constants.SvdData, 'a') 
        cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                          password=constants.DBPassword,
                                          database=constants.MainDB,
                                          host=constants.MainDBHost)
        cursor_main_db = cnx_main_db.cursor()
        cursor_main_db.execute("use " + constants.MainDB)
        query_text = "SELECT id FROM users" + ";"
        user_ids = make_query(query_text, cursor_main_db, cnx_main_db)
        for user_id in list(set(user_ids)):
            query_text = "SELECT venue_id FROM orders where user_id=" + "\'" + str(user_id) + "\'" + ";"
            venue_ids = make_query(query_text, cursor_main_db, cnx_main_db)
            for venue_id in list(set(venue_ids)):
                rating = test_distance_rating(user_id, venue_id)
                #rating = venue_ids.count(venue_id)
                svd.add_tuple((int(rating), int(user_id), int(venue_id)))
        svd.compute(k=100, min_values=1, pre_normalize=None, mean_center=True, post_normalize=True, savefile=constants.SvdData)
    else:
        svd = SVD(filename=constants.SvdData + "weighted_tag")
        cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                          password=constants.DBPassword,
                                          database=constants.MainDB,
                                          host=constants.MainDBHost)
        cursor_main_db = cnx_main_db.cursor()
        cursor_main_db.execute("use " + constants.MainDB)

        query_text = "SELECT id FROM users" + ";"
        user_ids = make_query(query_text, cursor_main_db, cnx_main_db)
        for user_id in list(set(user_ids)):
            query_text = "SELECT venue_id FROM orders where user_id=" + "\'" + str(user_id) + "\'" + ";"
            venue_ids = make_query(query_text, cursor_main_db, cnx_main_db)
            for venue_id in list(set(venue_ids)):
                #rating =
                rating = venue_ids.count(venue_id)
                #rating = test_distance_rating(user_id, venue_id)
                svd.add_tuple((int(rating), int(user_id), int(venue_id)))
    svd.create_matrix()
    return svd


def get_matr_count_tags():
    svd = SVD()
    if not os.path.isfile(constants.SvdData + "_tag.zip"):
        cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                          password=constants.DBPassword,
                                          database=constants.MainDB,
                                          host=constants.MainDBHost)
        cursor_main_db = cnx_main_db.cursor()
        cursor_main_db.execute("use " + constants.MainDB)
        
        users_tags = get_all_users_tags()
        venues_tags = get_all_venues_tags()
        for user in users_tags:
            for venue in venues_tags:
                rating = len(list(set(users_tags[user]).intersection(venues_tags[venue])))
                #if rating:
                svd.add_tuple((int(rating), int(user), int(venue)))
        svd.compute(k=100, min_values=0, pre_normalize=None, mean_center=True, post_normalize=True, savefile=constants.SvdData + "_tag") 
    else:
        svd = SVD(filename=constants.SvdData + "_tag")
        cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                          password=constants.DBPassword,
                                          database=constants.MainDB,
                                          host=constants.MainDBHost)
        cursor_main_db = cnx_main_db.cursor()
        cursor_main_db.execute("use " + constants.MainDB)
        users_tags = get_all_users_tags()
        venues_tags = get_all_venues_tags()
        ratings = {}
        for user in users_tags:
            for venue in venues_tags:
                rating = len(list(set(users_tags[user]).intersection(venues_tags[venue])))
                ratings[rating] = [user, venue]
                #if rating:
                svd.add_tuple((int(rating), int(user), int(venue)))
    svd.create_matrix()
    return svd


def get_matr_count_venues():
    svd = SVD()
    if not os.path.isfile(constants.SvdData + ".zip"):
#       svd_data = open(constants.SvdData, 'a') 
        cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                          password=constants.DBPassword,
                                          database=constants.MainDB,
                                          host=constants.MainDBHost)
        cursor_main_db = cnx_main_db.cursor()
        cursor_main_db.execute("use " + constants.MainDB)
        query_text = "SELECT user_id, venue_id, COUNT(*) as count FROM orders GROUP BY user_id, venue_id ORDER BY user_id DESC;"
        ratings = make_query(query_text, cursor_main_db, cnx_main_db)
        index = 0
        while index < len(ratings):
            user_id = ratings[index]
            venue_id = ratings[index + 1]
            rating = ratings[index + 2]
            if rating != 0:
                svd.add_tuple((int(rating), int(user_id), int(venue_id)))
            index += 3
        svd.compute(k=100, min_values=1, pre_normalize=None, mean_center=True, post_normalize=True, savefile=constants.SvdData)
    else:    
        svd = SVD(filename=constants.SvdData)
        cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                          password=constants.DBPassword,
                                          database=constants.MainDB,
                                          host=constants.MainDBHost)
        cursor_main_db = cnx_main_db.cursor()
        cursor_main_db.execute("use " + constants.MainDB)
        query_text = "SELECT user_id, venue_id, COUNT(*) as count FROM orders GROUP BY user_id, venue_id ORDER BY user_id DESC;"
        ratings = make_query(query_text, cursor_main_db, cnx_main_db)
        index = 0
        while index < len(ratings):
            user_id = ratings[index]
            venue_id = ratings[index + 1]
            rating = ratings[index + 2]
            svd.add_tuple((int(rating), int(user_id), int(venue_id)))
            index += 3
    svd.create_matrix()
    return svd
                

def cf_weighted_tag_number_recommend(user_id, n_venues=3):
    svd = get_matr_count_weighted_tags()
    best_venues = svd.recommend(i=int(user_id), is_row=True, only_unknowns=True)
    venues_to_return = []
    for venue in best_venues:
        venues_to_return.append(venue[0])
    #svd.predict(ITEMID, USERID, MIN_RATING, MAX_RATING)
    return venues_to_return


def cf_tag_number_recommend(user_id, n_venues=3):
    svd = get_matr_count_tags()
    best_venues = svd.recommend(i=int(user_id), is_row=True, only_unknowns=True)
    venues_to_return = []
    for venue in best_venues:
        venues_to_return.append(venue[0])
    #svd.predict(ITEMID, USERID, MIN_RATING, MAX_RATING)
    return venues_to_return


def cf_venue_number_recommend(user_id, n_venues=3):
    svd = get_matr_count_venues()
    best_venues = svd.recommend(i=int(user_id), is_row=True, only_unknowns=True)
    venues_to_return = []
    for venue in best_venues:
        venues_to_return.append(venue[0])
    #svd.predict(ITEMID, USERID, MIN_RATING, MAX_RATING)
    return venues_to_return


def get_all_venues_tags():
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)
    query_text = "SELECT DISTINCT mi.id as item_id, v.id as venue_id FROM menu_items as mi inner join venues as v on mi.venue_id=v.id  WHERE venue_id in (select venue_id from orders);"
    item_venue = make_query(query_text, cursor_main_db, cnx_main_db)

    query_text = "SELECT DISTINCT tag_id, item_id FROM menu_item_tags WHERE item_id IN (SELECT id FROM menu_items WHERE venue_id in (select venue_id from orders));"
    tag_item = make_query(query_text, cursor_main_db, cnx_main_db)
    item_venue_index = 0
    venue_to_items_dict = {}
    while item_venue_index < len(item_venue):
       venue_to_items_dict[item_venue[item_venue_index + 1]] = []
       item_venue_index += 2
    item_venue_index = 0
    while item_venue_index < len(item_venue):
       venue_to_items_dict[item_venue[item_venue_index + 1]].append(item_venue[item_venue_index])
       item_venue_index += 2
    
    item_to_tags_dict = {}
    tag_item_index = 0
    while tag_item_index < len(tag_item):
        item_to_tags_dict[tag_item[tag_item_index + 1]] = []
        tag_item_index += 2
    tag_item_index = 0
    while tag_item_index < len(tag_item):
        item_to_tags_dict[tag_item[tag_item_index + 1]].append(tag_item[tag_item_index])
        tag_item_index += 2
    venue_tags = {}
    for venue in venue_to_items_dict:
        for item in venue_to_items_dict[venue]:
            venue_tags[venue] = []
    for venue in venue_to_items_dict:
        for item in venue_to_items_dict[venue]:
            try:
                venue_tags[venue] += item_to_tags_dict[item]
            except Exception as e:
                pass
    for venue in venue_tags:
        venue_tags[venue] = list(set(venue_tags[venue]))
    return venue_tags


def make_dict(list_data):
    list_data_dict = {}    

    list_data_index = 0
    while list_data_index < len(list_data): # Key is a user, value is an order.
        list_data_dict[list_data[list_data_index]] = []
        list_data_index += 2
    
    list_data_index = 0
    while list_data_index < len(list_data): # Key is a user, value is an order.
        list_data_dict[list_data[list_data_index]].append(list_data[list_data_index + 1])
        list_data_index += 2
    
    return list_data_dict


def merge_dicts(dict1, dict2): # dict1 key -> dict2 values. Like a_to_b dict and b_to_c_dict -> a_to_c dict.
    merged_dict = {}
    for dict1_key in dict1:
        merged_dict[dict1_key] = []
    for dict1_key in dict1:
        for dict1_value in dict1[dict1_key]:
            if dict1_value in dict2:
                merged_dict[dict1_key] += dict2[dict1_value]
    for merged_dict_key in merged_dict:
        merged_dict[merged_dict_key] = list(set(merged_dict[merged_dict_key]))
    return merged_dict


def get_all_users_tags():
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)
    query_text = "SELECT DISTINCT user_id, id FROM orders;"
    user_order = make_query(query_text, cursor_main_db, cnx_main_db)
    query_text = "SELECT DISTINCT order_id, position_id FROM order_menu_positions;"
    order_position = make_query(query_text, cursor_main_db, cnx_main_db)
    query_text = "SELECT DISTINCT id, menu_item_id FROM menu_positions;"
    position_item = make_query(query_text, cursor_main_db, cnx_main_db)
    query_text = "SELECT DISTINCT item_id, tag_id FROM menu_item_tags;"
    item_tag = make_query(query_text, cursor_main_db, cnx_main_db)

    user_to_order = make_dict(user_order)
    order_to_position = make_dict(order_position)
    position_to_item = make_dict(position_item)
    item_to_tag = make_dict(item_tag)

    user_to_position = merge_dicts(user_to_order, order_to_position)
    user_to_item = merge_dicts(user_to_position, position_to_item)
    user_to_tag = merge_dicts(user_to_item, item_to_tag)
    
    return user_to_tag


def get_user_venue_tags_rating(user_id, venue_id):
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)
#    query_text = "SELECT tag_id FROM menu_item_tags WHERE item_id IN (SELECT menu_item_id FROM menu_positions WHERE id IN (SELECT position_id FROM order_menu_positions WHERE order_id IN (SELECT id FROM orders where user_id=" + str(user_id) + ")));"
#    user_tags = make_query(query_text, cursor_main_db, cnx_main_db)
#    query_text = "SELECT tag_id FROM menu_item_tags WHERE item_id in (SELECT id FROM menu_items WHERE venue_id=" + str(venue_id) + ");"
#    venue_tags = make_query(query_text, cursor_main_db, cnx_main_db)
#    tags = list(set(user_tags) & set(venue_tags))
    
    venue_tags = get_all_venues_tags()
    user_tags = get_all_users_tags()
    
    rating_matrix = []
    
    
    return len(tags)


def get_user_tags_amount(user_id):
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)
    query_text = "SELECT tag_id from menu_item_tags WHERE item_id IN (SELECT menu_item_id FROM menu_positions WHERE id IN (SELECT position_id FROM order_menu_positions WHERE order_id IN (SELECT id FROM orders WHERE user_id=" + str(user_id) + ")));"
    user_tags = make_query(query_text, cursor_main_db, cnx_main_db)
    tags_amount = {}
    for tag in list(set(user_tags)):
        tags_amount[tag] = user_tags.count(tag)
    return tags_amount


def get_all_venues_by_orders():
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)
    query_text = "SELECT venue_id FROM orders;"
    venue_ids = make_query(query_text, cursor_main_db, cnx_main_db)
    return venue_ids


def get_venues_tags_amount():
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)

    query_text = "SELECT DISTINCT mi.id as item_id, v.id as venue_id FROM menu_items as mi inner join venues as v on mi.venue_id=v.id  WHERE venue_id in (SELECT DISTINCT id from venues);"
    item_venue = make_query(query_text, cursor_main_db, cnx_main_db)

    query_text = "SELECT tag_id, item_id FROM menu_item_tags WHERE item_id IN (SELECT DISTINCT id FROM menu_items WHERE venue_id in (SELECT DISTINCT id from venues));"
    tag_item = make_query(query_text, cursor_main_db, cnx_main_db)
    item_venue_index = 0
    venue_to_items_dict = {}
    while item_venue_index < len(item_venue):
       venue_to_items_dict[item_venue[item_venue_index + 1]] = []
       item_venue_index += 2
    item_venue_index = 0
    while item_venue_index < len(item_venue):
       venue_to_items_dict[item_venue[item_venue_index + 1]].append(item_venue[item_venue_index])
       item_venue_index += 2

    item_to_tags_dict = {}
    tag_item_index = 0
    while tag_item_index < len(tag_item):
        item_to_tags_dict[tag_item[tag_item_index + 1]] = []
        tag_item_index += 2
    tag_item_index = 0
    while tag_item_index < len(tag_item):
        item_to_tags_dict[tag_item[tag_item_index + 1]].append(tag_item[tag_item_index])
        tag_item_index += 2
    venue_tags = {}
    for venue in venue_to_items_dict:
        for item in venue_to_items_dict[venue]:
            venue_tags[venue] = []
    for venue in venue_to_items_dict:
        for item in venue_to_items_dict[venue]:
            try:
                venue_tags[venue] += item_to_tags_dict[item]
            except Exception as e:
                pass
    venues_to_tags_dict = {}
    for venue in venue_tags:
        venues_to_tags_dict[venue] = {}
        for tag in list(venue_tags[venue]):
            venues_to_tags_dict[venue][tag] = venue_tags[venue].count(tag)
    return venues_to_tags_dict

    



#    venue_ids = get_all_venues_by_orders()
#    venue_tags_amount = {}
#    for venue_id in list(set(venue_ids)):
#        offer_tags = []
#        query_text = "SELECT id FROM orders where venue_id=" + "\'" + str(venue_id) + "\'" + ";"
#        order_ids = make_query(query_text, cursor_main_db, cnx_main_db)
#        for order_id in order_ids:
#            query_text = "SELECT position_id FROM order_menu_positions where order_id=" + "\'" + str(order_id) + "\'" + ";"
#            position_ids = make_query(query_text, cursor_main_db, cnx_main_db)
#            for position_id in position_ids:
#                query_text = "SELECT menu_item_id FROM menu_positions where id=" + "\'" + str(position_id) + "\'" + ";"
#                menu_item_ids = make_query(query_text, cursor_main_db, cnx_main_db)
#                for menu_item_id in menu_item_ids:
#                    query_text = "SELECT tag_id FROM menu_item_tags where item_id=" + "\'" + str(menu_item_id) + "\'" + ";"
#                    tag_ids = make_query(query_text, cursor_main_db, cnx_main_db)
#                    for tag_id in tag_ids:
#                        query_text = "SELECT content FROM tags where id=" + "\'" + str(tag_id) + "\'" + ";"
#                        offer_tags += make_query(query_text, cursor_main_db, cnx_main_db)
#        tags_amount = {}
#        for tag in list(set(offer_tags)):
#            tags_amount[tag] = offer_tags.count(tag)
#        venue_tags_amount[venue_id] = tags_amount
#    return venue_tags_amount


def get_statistics(user_id):
    user_tags_statistics = get_user_tags_amount(user_id)
    tag_sum = float(len(user_tags_statistics))
    for tag in user_tags_statistics:
        user_tags_statistics[tag] /= tag_sum
    venue_tags_statistics = get_venues_tags_amount()
    for venue_id in venue_tags_statistics:
        tag_sum = float(len(venue_tags_statistics[venue_id]))
        for tag in venue_tags_statistics[venue_id]:
            venue_tags_statistics[venue_id][tag] /= tag_sum
    statistics = {'user': user_tags_statistics, 'venues': venue_tags_statistics}
    return statistics


def get_user_venues(user_id):
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)
    query_text = "SELECT venue_id FROM orders where user_id=" + "\'" + str(user_id) + "\'" + ";"
    venue_ids = make_query(query_text, cursor_main_db, cnx_main_db)
    return list(set(venue_ids))


def item_based_recommend(user_id):
    top_venues = []
    top_recommend = {}
    statistics = get_statistics(user_id) # In future safe to file and get data from periodically updating file.
    user_tags = statistics['user']
    user_keys = set(user_tags.keys())
    for venue_id in statistics['venues']:
        top_venues.append(venue_id)
        venue_tags = statistics['venues'][venue_id]
        venues_keys = set(venue_tags.keys())
        intersection_tags = list(user_keys & venues_keys)
        user_rating = []
        venue_rating = []
        for tag in intersection_tags:
            user_rating.append(user_tags[tag])
            venue_rating.append(venue_tags[tag])
            
        dst = distance.euclidean(user_rating, venue_rating)
        top_recommend[venue_id] = dst
    #max_keys = max(top_recommend.iteritems(), key=operator.itemgetter(1))
    max_keys = sorted(top_recommend, key=top_recommend.get, reverse=True)
    zero_index = 0
    while top_recommend[max_keys[zero_index]]:
        zero_index += 1
    user_venues = get_user_venues(user_id)
    max_keys = max_keys[:zero_index]
    recommend_venues = []
    for venue_id in max_keys:
        if venue_id not in user_venues:
            recommend_venues.append(venue_id)
    return recommend_venues


def test_distance_rating(user_id, venue_id): # Distance by weighted tags
    top_venues = []
    top_recommend = {}
    statistics = get_statistics(user_id) # In future safe to file and get data from periodically updating file.
    user_tags = statistics['user']
    user_keys = set(user_tags.keys())
    top_venues.append(venue_id)
    venue_tags = statistics['venues'][venue_id]
    venues_keys = set(venue_tags.keys())
    intersection_tags = list(user_keys & venues_keys)
    user_rating = []
    venue_rating = []
    for tag in intersection_tags:
        user_rating.append(user_tags[tag])
        venue_rating.append(venue_tags[tag])
    dst = distance.euclidean(user_rating, venue_rating)
    return dst

