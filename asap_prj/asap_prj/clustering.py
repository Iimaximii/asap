import numpy as np
from math import radians, cos, sin, asin, sqrt, acos, floor, degrees as to_degrees
from sklearn import metrics
from sklearn.cluster import KMeans, DBSCAN
from constants import MinutesPerHour, HoursPerDay, NoiseIndex, RandomSeed, HourMinuteDelimiter


def board_case(time_data):
    less_elem_exist = False
    more_elem_exist = False
    high_board = 18
    low_board = 6
    for time_entry in time_data:
        if time_entry > high_board * MinutesPerHour:
            more_elem_exist = True
        if time_entry < low_board * MinutesPerHour:
            less_elem_exist = True
    return less_elem_exist and more_elem_exist


def cluster_time(data, original_time):
    formated_data = format_data(data)
    np.random.seed(RandomSeed)
    estimator_min_distance_to_clusterize = 0.03
    estimator_euclidean_metric = 'euclidean'
    estimator = DBSCAN(eps = estimator_min_distance_to_clusterize, metric = estimator_euclidean_metric)
    estimator.fit(formated_data)
    labels = (estimator.labels_).tolist()
    label_time = {}
    for label in list(set(labels)):
        if label != NoiseIndex:
            label_time[label] = []
    current_label_index = 0 
    while current_label_index < len(labels):
        if labels[current_label_index] != NoiseIndex:
            label_time[labels[current_label_index]].append(original_time[current_label_index])
        current_label_index += 1
    centers = [None] * len(label_time)
    for label in label_time:
        mean_deg = 0.0
        times_summed = 0.0
        sum_list = []
        for time in label_time[label]:
            parsed_time = time.split(HourMinuteDelimiter)
            time_in_minutes = int(parsed_time[0]) * MinutesPerHour + int(parsed_time[1])
            sum_list.append(time_in_minutes)
        if board_case(sum_list):
            for sum_elem in sum_list:
                if sum_elem < 6 * MinutesPerHour:
                    sum_elem += HoursPerDay * MinutesPerHour
                mean_deg += sum_elem
        else:
            for sum_elem in sum_list:
                mean_deg += sum_elem
#           parsed_time = time.split(":")
#           to_add = int(parsed_time[0]) * 60 + int(parsed_time[1])
#           if times_summed != 0:
#               
                                
#           mean_deg += int(parsed_time[0]) * 60 + int(parsed_time[1])
#           times_summed += 1
        mean_deg = (mean_deg / len(sum_list)) % (HoursPerDay * MinutesPerHour)
        hours = int(mean_deg / MinutesPerHour)
        mins = int(mean_deg - hours * MinutesPerHour)
        min_hours_to_add_zero = 10
        zero = '0'
        if hours < min_hours_to_add_zero:
            hours = zero + str(hours)
        if mins < min_hours_to_add_zero:
            mins = zero + str(mins)
        centers[label] = str(hours) + HourMinuteDelimiter + str(mins)
    labels_centers = {'labels': labels, 'centers': centers}
    return labels_centers


def cluster_geo(data):
    formated_data = format_data(data)
    np.random.seed(RandomSeed)
    num_clusters = min(8, len(data))
    estimator_min_distance_to_clusterize = 0.001
    estimator_euclidean_metric = 'euclidean'
    estimator = DBSCAN(eps = estimator_min_distance_to_clusterize, metric = estimator_euclidean_metric)
    estimator.fit(formated_data)
    labels = (estimator.labels_).tolist()
    centers_dict = {}
    for lab in set(labels):
        centers_dict[lab] = [0, 0]
    data_index = 0
    while data_index < len(data):
        centers_dict[labels[data_index]][0] += data[data_index][0]
        centers_dict[labels[data_index]][1] += data[data_index][1]
        data_index += 1
    for label_index in centers_dict:
        centers_dict[label_index][0] /= float(labels.count(label_index))
        centers_dict[label_index][1] /= float(labels.count(label_index))
    centers = []
    if NoiseIndex in labels:
        centers = [None] * (len(set(labels)) - 1)
        for empty_center in centers_dict:
            if empty_center != NoiseIndex:
                centers[empty_center] = centers_dict[empty_center]
    else:
        centers = [None] * len(set(labels))
        for empty_center in centers_dict:
            centers[empty_center] = centers_dict[empty_center]
    labels_centers = {'labels': labels, 'centers': centers}
    return labels_centers


def format_data(data):
    formated_data = []
    for entry in data:
        formated_data.append(np.array(entry))
    return np.array(formated_data)


def get_cluster_size(data, labels):
    i = 0
    sizes = {}
    while i < len(labels):
        sizes[labels]
        i += 1
