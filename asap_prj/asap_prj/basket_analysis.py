from apyori import apriori
import mysql.connector
import constants as constants
from ast import literal_eval


def make_query(query_text, cursor, cnx):
    query = (query_text)
    data = []
    try:
        cursor.execute(query)
    except Exception as e:
        pass
    try:
        for row in cursor:
            for col in row:
                data.append(col)
    except Exception as e:
        pass
    try:
        cnx.commit()
    except Exception as e:
        pass
    return data


def make_dict(list_data):
    list_data_dict = {}

    list_data_index = 0
    while list_data_index < len(list_data): # Key is a user, value is an order.
        list_data_dict[list_data[list_data_index]] = []
        list_data_index += 2

    list_data_index = 0
    while list_data_index < len(list_data): # Key is a user, value is an order.
        list_data_dict[list_data[list_data_index]].append(list_data[list_data_index + 1])
        list_data_index += 2

    return list_data_dict


def merge_dicts(dict1, dict2): # dict1 key -> dict2 values. Like a_to_b dict and b_to_c_dict -> a_to_c dict.
    merged_dict = {}
    for dict1_key in dict1:
        merged_dict[dict1_key] = []
    for dict1_key in dict1:
        for dict1_value in dict1[dict1_key]:
            if dict1_value in dict2:
                merged_dict[dict1_key] += dict2[dict1_value]
    for merged_dict_key in merged_dict:
        merged_dict[merged_dict_key] = list(set(merged_dict[merged_dict_key]))
    return merged_dict


def get_all_transactions():
    cnx_main_db = mysql.connector.connect(user=constants.DBUsername,
                                      password=constants.DBPassword,
                                      database=constants.MainDB,
                                      host=constants.MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + constants.MainDB)

    query_text = "SELECT order_id, position_id FROM order_menu_positions;"
    order_position = make_query(query_text, cursor_main_db, cnx_main_db)

    query_text = "SELECT id, menu_item_id FROM menu_positions;"
    position_item = make_query(query_text, cursor_main_db, cnx_main_db)
    
    order_to_position_dict = make_dict(order_position)
    position_to_item_dict = make_dict(position_item)

    order_to_items_dict = merge_dicts(order_to_position_dict, position_to_item_dict)
    keys_to_delete = []
    for order in order_to_items_dict:
        if order_to_items_dict[order] == []:
            keys_to_delete.append(order)
    for order in keys_to_delete:
        del order_to_items_dict[order]

    all_transactions = []
    ones = 0
    zeros = 0
    for order in order_to_items_dict:
        all_transactions.append(order_to_items_dict[order])
    for order in all_transactions:
        if len(order) == 1:
            ones += 1
        if len(order) == 0:
            zeros += 1
    qqq = float(ones) / float(len(all_transactions))
    sss = len(all_transactions)
    return all_transactions


def get_items_apriori(transaction_list):
    transaction_list = literal_eval(transaction_list)
    index = 0
    all_transactions = get_all_transactions()
    results = list(apriori(all_transactions, min_support=0.001, min_confidence=0.001, min_lift=0.001))
    result_list = []
    for res in results:
        for item in transaction_list:
            if item not in res[0]:
                break
            result_list += list(set(res[0]) - (set(res[0]) & set(transaction_list)))
    result_list = list(set(result_list))
    return result_list

