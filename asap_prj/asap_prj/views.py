from django.http import HttpResponse
from django.shortcuts import loader
from django.contrib.auth.models import User
from django.template import RequestContext
# from django.core.context_processors import csrf
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

is_logged = False;

def is_logged():
    return is_logged


def index(request):
    return HttpResponse(loader.get_template('site/market.html').render(RequestContext(request, {})))
#    return HttpResponse(loader.get_template('site/index.html').render(RequestContext(request, {})))

def register(request):
    if request.method == 'POST':
        return
    else:
        return HttpResponse(loader.get_template('site/register.html').render(RequestContext(request, {})))

def signin(request):
    if request.method == 'POST':
        return
    return HttpResponse(loader.get_template('site/signin.html').render(RequestContext(request, {})))
