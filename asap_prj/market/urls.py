from django.conf.urls import url

from views import *

urlpatterns = [
    url(r'^$', index, name='index_market'),
    # url(r'^venues/', venues),
    # url(r'^clients/', clients),
    url(r'^create/', create, name='create'),
    url(r'^campaign_preview/', campaign_preview, name='campaign_preview'),
    url(r'^current_campaigns/', current_campaigns, name='current_campaigns'),
    url(r'^campaign_details/', campaign_details, name='campaign_details'),
    url(r'^event/', event, name='event'),
    url(r'^campaign_menu/', campaign_menu, name='campaign_menu'),
    url(r'^venue/', venue, name='venue'),
    url(r'^analytics/', analytics, name='analytics'),
    url(r'^recommend/', recommend, name='recommend'),
    url(r'^recommend_items_to_order/', recommend_items_to_order, name='recommend_items_to_order'),
    url(r'^events/', event_log, name='event_log'),
]
