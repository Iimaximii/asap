# -*- coding: utf-8 -*-

from django.http import HttpResponse
import json
import requests
import logging
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.shortcuts import loader
from django.template import RequestContext
import mysql.connector
import datetime
from math import radians, cos, sin, asin, sqrt, acos, floor, degrees as to_degrees
from ast import literal_eval
from database import *
from django.views.decorators.http import require_http_methods
from asap_prj.constants import *
from mysql.connector import errorcode
from django.views.decorators.csrf import csrf_exempt
import uuid
import psycopg2
from psycopg2.extensions import AsIs
from asap_prj.clustering import cluster_geo, cluster_time
from asap_prj.recommendations import cf_venue_number_recommend, cf_tag_number_recommend, cf_weighted_tag_number_recommend, item_based_recommend
from asap_prj.basket_analysis import get_items_apriori
import numpy as np
from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import render
from django.shortcuts import render_to_response
import ast
from django.http import HttpResponseRedirect


logger = logging.getLogger(__name__)


# try:
#cnx_main_db = mysql.connector.connect(user = DBUsername,
#                                          password = DBPassword,
#                                          database = MainDB,
#                                          host=MainDBHost)
# except mysql.connector.Error as err:
#     if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
#         print("Something is wrong with your user name or password")
#         print (err)
#     elif err.errno == errorcode.ER_BAD_DB_ERROR:
#         print("Database does not exist")
#     else:
#         print (err)
#cursor_main_db = cnx_main_db.cursor()
#cursor_main_db.execute("use " + MainDB)


# try:
#cnx_campaigns_db = psycopg2.connect(database=MarketingDBName,
#                                    user=MarketingDBUserName,
#                                    password=MarketingDBPassword,
#                                    host=MarketingDBHost,
#                                    port=MarketingDBPort)
#cnx_campaigns_db = mysql.connector.connect(user=constants.DBUsername,
#                                  password=constants.DBPassword,
#                                  database=constants.CampaignsDB,
#                                  host='localhost')
# except mysql.connector.Error as err:
#     if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
#         print("Something is wrong with your user name or password")
#         print (err)
#     elif err.errno == errorcode.ER_BAD_DB_ERROR:
#         print("Database does not exist")
#     else:
#         print (err)
#cursor_campaigns_db = cnx_campaigns_db.cursor()
#cursor_campaigns_db.execute("use " + constants.CampaignsDB)


def open_campaigns_db_connection():
    cnx_campaigns_db = psycopg2.connect(database=MarketingDBName,
                                    user=MarketingDBUserName,
                                    password=MarketingDBPassword,
                                    host=MarketingDBHost,
                                    port=MarketingDBPort)
    cursor_campaigns_db = cnx_campaigns_db.cursor()
    return {'cnx': cnx_campaigns_db, 'cursor': cursor_campaigns_db}


def open_main_db_connection():
    cnx_main_db = mysql.connector.connect(user = DBUsername,
                                          password = DBPassword,
                                          database = MainDB,
                                          host=MainDBHost)
    cursor_main_db = cnx_main_db.cursor()
    cursor_main_db.execute("use " + MainDB) 
    return {'cnx': cnx_main_db, 'cursor': cursor_main_db}


def close_connection(cursor):
    cursor.close()
    return


def make_query(query_text, cursor, cnx):
    query = (query_text)
    data = []
    try:
        cursor.execute(query)
    except Exception as e:
        pass
    try:
        for row in cursor:
            for col in row:
                data.append(col)
    except Exception as e:
        pass
    try:
        cnx.commit()
    except Exception as e:
        pass
    return data


@csrf_exempt
def index(request):
    if request.method == 'POST':
        if 'mail' in request.POST and 'pwd' in request.POST and 'conf_pwd' in request.POST:
            if request.POST['pwd'] == request.POST['conf_pwd']:
                try:
                    user = User.objects.create_user(username=request.POST['mail'],
                                                    email=request.POST['mail'],
                                                    password=request.POST['pwd'])
                    user.save()
                except:
                    return HttpResponse(loader.get_template('site/register.html').render(RequestContext(request,
                                                                                              {'empty_field': 1})))
                return HttpResponse(loader.get_template('site/market.html').render(RequestContext(request,
                                                                                                  {'status_code': 1})))
            else:
                return HttpResponse(loader.get_template('site/register.html').render(RequestContext(request,
                                                                                              {'wrong_conf': 1})))
        if 'mail' in request.POST and 'pwd' in request.POST and 'conf_pwd' not in request.POST:
#            try:
            #authenticate(username=request.GET['mail'], email=request.POST['mail'], password=request.GET['pwd'])
            return HttpResponse(loader.get_template('site/market.html').render(RequestContext(request,
                                                                                              {'status_code': 1})))
#            except:
#                return HttpResponse(loader.get_template('site/signin.html').render(RequestContext(request,
#                                                                                          {'not_registered': 1})))
        camp_conn = open_campaigns_db_connection()
        if request.POST['new_campaign'] == 'true':
            query = "SELECT * FROM campaigns WHERE campaign_name=" + "\'" + request.POST['campaign_name'] + "\'" + ";"
            if make_query(query, camp_conn['cursor'], camp_conn['cnx']):
                return HttpResponse(loader.get_template('site/market.html').
                                render(RequestContext(request, {"status_code": 0})))
            connection = open_main_db_connection()
            query = "SELECT id FROM venues WHERE name=" + "\'" + request.POST['venue'] + "\'" + ";"

            venue_id = make_query(query, connection['cursor'], connection['cnx'])

            camp_conn['cursor'].execute("""INSERT INTO campaigns (status, radius_meters, date_time, campaign_name, venue_id, venue_name, message, content) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""", 
                ('active', request.POST['distance'], str(datetime.datetime.now().strftime("%Y-%m-%d %H:%m")), request.POST['campaign_name'], 
                str(venue_id[0]), request.POST['venue'], request.POST['message'], request.POST['content']))
      #      query = "INSERT INTO campaigns (status, radius_meters, date_time, campaign_name, venue_id, venue_name," \
     #               " message, content) " \
    #                "VALUES (" + '\'' + "active" + '\'' + "," + '\'' + request.POST['distance'] + '\'' "," + \
   #                 '\'' + str(datetime.datetime.now()) + '\'' + "," + \
  #                  '\'' + request.POST['campaign_name'] + '\'' + "," + '\'' + str(venue_id[0]) + '\'' + "," +\
 #                   '\'' + request.POST['venue'] + '\'' + "," + '\'' + request.POST['message'] + '\'' + "," +\
#                   '\'' + unicode(str(literal_eval(request.POST['content'])), 'unicode-escape').replace('\'', '\'\'') + '\''  + ");"
                    #'\'' + str(cont) + '\'' + ");"
#                   '\'' + str(cont) + '\'' + ");"
                    #'\'' + unicode(str(literal_eval(request.POST['content'])), 'unicode-escape') + '\''  + ");"
#                   '\'' + str(cont) + '\'' + ");"
#            make_query(query, cursor_campaigns_db, cnx_campaigns_db)
            camp_conn['cnx'].commit()
            close_connection(camp_conn['cursor'])
            close_connection(connection['cursor'])
            return render(request, 'site/market.html', {"status_code": 1})
            # render_to_response('site/market.html', context_instance=RequestContext(request, {"status_code": 1}))
        # else:
        #     args = {'user_id': int(415), 'content': "Welcome to ASAP app.", "datetime": datetime.datetime.now().
        #             strftime("%y:%m:%d %H:%M:%S")}
        #     try:
        #         r = requests.get('https://asapme.club/api/notification/register/', params=args, timeout=5)
        #     return HttpResponse(loader.get_template('site/market.html').render(RequestContext(request, {})))
    return HttpResponse("Something gone really wrong")


def campaign_preview(request):
    if request.method == 'GET':  # Venue distance message.
        campaign_name = request.GET['campaign_name']
        venue_name = request.GET['venue']
        distance = request.GET['distance']
        message = request.GET['message']
        items = request.GET['item']
        bracket_index = -1
        while bracket_index != 0:
            if items[bracket_index] == '(':
                items = items[:bracket_index - 1]
                bracket_index = 0
            else:
                bracket_index -= 1
        connection = open_main_db_connection()
        query_text = "SELECT id FROM venues WHERE name=" + "\'" + venue_name + "\'" + ";"
        venue_id = make_query(query_text, connection['cursor'], connection['cnx'])[0]
        query_text = "SELECT id FROM menu_items WHERE venue_id="+ "\'" + str(venue_id) + "\' " + "and " +\
                     "name=" + "\'" + items + "\'" + ";"
        item_id = make_query(query_text, connection['cursor'], connection['cnx'])
        contents = []
        for it_id in item_id:
            query_text = "SELECT tag_id FROM menu_item_tags WHERE item_id=" + "\'" + str(it_id) + "\'" + ";"
            tags = make_query(query_text, connection['cursor'], connection['cnx'])
            for tag_id in tags:
                query_text = ("SELECT content FROM tags WHERE id=" + "\'" + str(tag_id) + "\'" + ";")
                contents.append(make_query(query_text, connection['cursor'], connection['cnx'])[0])
        close_connection(connection['cursor'])
        if not contents:
            return HttpResponse("Incorrect match. Try another combination.")
        return HttpResponse(loader.get_template('site/campaign_preview.html').render(RequestContext(request, {
            'campaign_name': campaign_name, 'venue': venue_name, 'distance': distance, 'message': message,
            'content': contents})))


@csrf_exempt
def create(request):
    venue = ""
    items = []
    if 'selected_item' in request.POST and 'selected_venue' in request.POST and 'distance' in request.POST and 'campaign_name' in request.POST and 'message' in request.POST:
        campaign_name = request.POST['campaign_name']
        distance = request.POST['distance']
        venue = request.POST['selected_venue']
        items = request.POST.getlist('selected_item')
        message = request.POST['message']
    else:
        return HttpResponse("Impossible!")
    connection = open_main_db_connection()
    venue_id = venue.split(" ")[0]
    index = 0
    while venue[index] != " ":
        index += 1
    venue_name = venue[index:]
    item_ids = []
    for item in items:
        item_ids.append(item.split(" ")[0])
    contents = []
    for it_id in item_ids:
        query_text = "SELECT tag_id FROM menu_item_tags WHERE item_id=" + "\'" + it_id + "\'" + ";"
        tags = make_query(query_text, connection['cursor'], connection['cnx'])
        for tag_id in tags:
            query_text = ("SELECT content FROM tags WHERE id=" + "\'" + str(tag_id) + "\'" + ";")
            contents.append(make_query(query_text, connection['cursor'], connection['cnx'])[0])
    close_connection(connection['cursor'])
    camp_conn = open_campaigns_db_connection()
    camp_conn['cursor'].execute("""INSERT INTO campaigns (status, radius_meters, date_time, campaign_name, venue_id, venue_name, message, content) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""",
    ('active', distance, str(datetime.datetime.now().strftime("%Y-%m-%d %H:%m")), campaign_name,
                venue_id, venue_name, message, str(contents)))
    camp_conn['cnx'].commit()
    close_connection(camp_conn['cursor'])


def current_campaigns(request):
    camp_conn = open_campaigns_db_connection()
    query_text = "SELECT campaign_name FROM campaigns;"
    data = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
    close_connection(camp_conn['cursor'])
    data = json.dumps(data)
    connection = open_main_db_connection()
    
    query_text = "SELECT id, name FROM venues;"
    id_name_venues = make_query(query_text, connection['cursor'], connection['cnx'])
    
    query_text = "SELECT DISTINCT id, name, venue_id, FROM menu_items;"
    id_name_venue_id_item = make_query(query_text, connection['cursor'], connection['cnx'])
    
    id_to_name_venues = {}

    venue_to_items = {}
    index = 0
    while index < len(id_name_venues):
        venue_id = str(id_name_venues[index])
        venue_name = str(id_name_venues[index + 1])
        id_to_name_venues[venue_id] = venue_name
        venue_to_items[venue_id + " " + venue_name] = []
        index += 2

    index = 0
    while index < len(id_name_venue_id_item):
        item_id = str(id_name_venue_id_item[index])
        item_name = str(id_name_venue_id_item[index + 1])
        venue_id = str(id_name_venue_id_item[index + 2])
        venue_to_items[venue_id + " " + id_to_name_venues[venue_id]] += item_id
        index += 3
    close_connection(connection['cursor'])
    return HttpResponse(loader.get_template('site/campaign_menu.html').render(RequestContext(request,
                                                                                                 {'data': data})))


def campaign_details(request):
    camp_conn = open_campaigns_db_connection()
    if 'remove_id' in request.GET:
        query_text = "DELETE FROM campaigns WHERE id=" + "\'" + str(request.GET['remove_id']) + "\'" + ";"
        make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
        camp_conn['cnx'].commit()
        return HttpResponseRedirect('http://ec2-52-49-133-55.eu-west-1.compute.amazonaws.com/market/campaign_menu/')
#        return campaign_menu(request)
#        return current_campaigns(request)
    query_text = "SELECT column_name FROM information_schema.columns WHERE table_name='campaigns';"
    headers = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
    query_text = "SELECT * FROM campaigns WHERE campaign_name=" + "\'" + request.GET['campaign_detailed'] + "\'" + ";"
    no_headers_data = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
    data = {}
    list_ind = 0
    while list_ind < len(headers):
        data[headers[list_ind]] = no_headers_data[list_ind]
        list_ind += 1
    #data['content'] = ['qwe', 'tag1']
    data['content'] = json.dumps(literal_eval(data['content']))
    close_connection(camp_conn['cursor'])
    return HttpResponse(loader.get_template('site/campaign_details.html').render(RequestContext(request, data)))


def log(request):
    try:
        r = requests.get('http://0.0.0.0:5000' + '/get_log', params={'venue': request.GET['venue_detailed']}, timeout=5)
    except Exception as e:
        logger.exception(e)
    log_info = literal_eval(r.content)
    return HttpResponse(loader.get_template('site/campaign_details.html').render(RequestContext(request, log_info)))


@csrf_exempt
def event(request):
    try:
        data = literal_eval(str(request.body))
        user_id = data['user_id']
        latitude = data['latitude']
        longitude = data['longitude']
        date_time = data['datetime'] 
    except Exception as e:
        try:
            user_id = request.POST['user_id']
            latitude = request.POST['latitude']
            longitude = request.POST['longitude']
            date_time = request.POST['datetime']
        except Exception as e:
            return HttpResponse(status=401)
    geo_data = {'latitude': [str(latitude)], 'longitude': [str(longitude)], 'datetime': [str(date_time)]}
#    update_geo_analytics(str(user_id), geo_data)
    if not time_to_send_push(str(user_id)):
        return HttpResponse(status=402)
    if send_push(user_id, latitude, longitude, date_time):
        register_last_push(str(user_id))
        new_id = uuid.uuid4()
        return HttpResponse(content=new_id, status=200)
    else:
        return HttpResponse(status=403)
    # new_id = make_query(query_text, constants.MainDB)[0]


def get_user_tags(user_id):
    connection = open_main_db_connection()
    query_text = "SELECT content FROM tags WHERE id IN (SELECT tag_id FROM menu_item_tags WHERE item_id IN (SELECT menu_item_id FROM menu_positions WHERE id IN (SELECT position_id FROM order_menu_positions where order_id IN (SELECT id FROM orders where user_id=" + str(user_id) + "))));"
    offer_tags = make_query(query_text, connection['cursor'], connection['cnx'])
    close_connection(connection['cursor'])
    return list(set(offer_tags))


def send_push(user_id, user_latitude, user_longitude, date_time):
    result = []
    camp_conn = open_campaigns_db_connection()
    query_text = "SELECT campaign_name FROM campaigns;"
    campaigns = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
    max_priority = -100000000
    args = {}
    connection = open_main_db_connection()
    for camp_name in campaigns:
        query_text = "SELECT column_name FROM information_schema.columns WHERE table_name='campaigns';"
        headers = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
        query_text = "SELECT * FROM campaigns WHERE campaign_name=" + "\'" + camp_name + "\'" + ";"
        no_headers_data = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
        campaign_data = {}
        list_ind = 0
        while list_ind < len(headers):
            campaign_data[headers[list_ind]] = no_headers_data[list_ind]
            list_ind += 1
        campaign_tags = literal_eval(campaign_data['content'])
        user_tags = get_user_tags(user_id)
        tags_to_offer = list(set(user_tags).intersection(campaign_tags))
        query_text = "SELECT lon FROM venues WHERE id=" + str(campaign_data['venue_id']) + ";"
        venue_longitude = make_query(query_text, connection['cursor'], connection['cnx'])[0]
        query_text = "SELECT lat FROM venues WHERE id=" + "\'" + str(campaign_data['venue_id']) + "\'" + ";"
        venue_latitude = make_query(query_text, connection['cursor'], connection['cnx'])[0]
        distance = get_distance_meters(float(venue_longitude), float(venue_latitude), float(user_longitude), float(user_latitude))
        if (distance <= campaign_data['radius_meters']) and tags_to_offer:
            if 50 * len(tags_to_offer) - distance > max_priority:
                result = []
                for offer_tag in tags_to_offer: # Some useless work here...
                    result.append(offer_tag)
                    args = {'user_id': int(user_id),
                            'content': campaign_data['message'],
                            "datetime": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%m:00"))}
                # push(args)
                # if push(args):
                    # add to log
                    # args = {'user_id': user_id, 'date_time': date_time, 'compaign_id': }
                    # try:
                    #     r = requests.post('http://0.0.0.0:5000' + '/add_campaign', params=args, timeout=5)
                    # except Exception as e:
                    #     logger.exception(e)

    # print "RESULT: "
    # result = list(set(result))
    close_connection(camp_conn['cursor'])
    close_connection(connection['cursor'])
    return push(args)#    opopopop


def push(args):
    try:
        requests.get('https://asapme.club/api/notification/register/', params=args)
    except Exception as e:
        return False
    return True


def time_to_send_push(user_id):
    connection = open_campaigns_db_connection()
    query_text = "SELECT date_time FROM last_push WHERE user_id=" + user_id + ";"
    last_time = make_query(query_text, connection['cursor'], connection['cnx'])
    close_connection(connection['cursor'])
    if last_time == []:
        return True;
    last_time = datetime.datetime.strptime(last_time[0], '%Y-%m-%d %H:%M:%S.%f')
    now = datetime.datetime.now()
#    delta = (now - last_time).total_seconds() / 3600.0
    delta = (now - last_time).total_seconds() / 60.0
    if delta >= 5:
        return True
    return False


def register_last_push(user_id):
    connection = open_campaigns_db_connection()
    query_text = "SELECT date_time FROM last_push WHERE user_id=" + '\'' + user_id + '\'' + ";"
    last_time = make_query(query_text, connection['cursor'], connection['cnx'])
    if not last_time:
        query_text = """INSERT INTO last_push (user_id, date_time) VALUES (%s, %s)"""
        params = (str(user_id), str(datetime.datetime.now()))
        connection['cursor'].execute(query_text, params)
#        cursor_campaigns_db.execute("""INSERT INTO last_push (user_id, date_time) VALUES (%s, %s)""", (user_id, str(datetime.datetime.now())))
    else:
        query_text = "UPDATE last_push SET date_time=" + '\'' + str(datetime.datetime.now()) + '\'' + "WHERE user_id=" + '\'' + user_id + '\''+ ";"
        make_query(query_text, connection['cursor'], connection['cnx'])
    query_text = """INSERT INTO all_push (user_id, date_time) VALUES (%s, %s)"""
    params = (str(user_id), str(datetime.datetime.now()))
    connection['cursor'].execute(query_text, params)
    connection['cnx'].commit()
    close_connection(connection['cursor'])
    return


def venue(request):
    try:
        data = literal_eval(str(request.body))
        user_id = data['user_id']
        latitude = data['latitude']
        longitude = data['longitude']
    except Exception as e:
        try:
            user_id = request.GET.get('user_id')
            latitude = literal_eval(request.GET.get('latitude'))
            longitude = literal_eval(request.GET.get('longitude'))
        except Exception as e:
            return HttpResponse(status=401)
    venues = get_venue_list(user_id, latitude, longitude)
    if not venues:
        return HttpResponse(status=402)
    return HttpResponse(str(venues), status=200)


def get_venue_list(user_id, latitude, longitude):
    visited_id = []
    connection = open_main_db_connection()
    if len(latitude) == len(longitude):
        i = 0
        query_text = "SELECT id FROM venues"
        venue_id = make_query(query_text, connection['cursor'], connection['cnx'])
        venue_longitude = []
        venue_latitude = []
        for ven_id in venue_id:
            query_text = "SELECT lon FROM venues WHERE id=" + str(ven_id) + ";"
            venue_longitude += make_query(query_text, connection['cursor'], connection['cnx'])
            query_text = "SELECT lat FROM venues WHERE id=" + "\'" + str(ven_id) + "\'" + ";"
            venue_latitude += make_query(query_text, connection['cursor'], connection['cnx'])
        while i < len(longitude):
            j = 0
            min_distance = 10
            lat = 0
            lon = 0
            while j < len(venue_latitude):
                new_distance = get_distance_meters(venue_longitude[j], venue_latitude[j], longitude[i], latitude[i])
                if new_distance < min_distance:
                    min_distance = new_distance
                    lat = venue_latitude[j]
                    lon = venue_longitude[j]
                j += 1
            if lon and lat:
                query_text = "SELECT id FROM venues WHERE lat=" + str(lat) + " AND lon=" + str(lon) + ";"
                visited_id += make_query(query_text, connection['cursor'], connection['cnx'])
            i += 1
    close_connection(connection['cursor'])
    return list(set(visited_id))


#def item(request):
#    try:
#        data = literal_eval(str(request.body))
#        user_id = data['user_id']
#        number = data['number']
#    except Exception as e:
#        try:
#            user_id = request.GET.get('user_id')
#            number = literal_eval(request.GET.get('number'))
#        except Exception as e:
#            return HttpResponse(status=400)
#    return HttpResponse(content=get_favourite_item(user_id, number), status=200)
 

def update_geo_analytics(user_id, data):
    i = 0
    camp_conn = open_campaigns_db_connection()
    while i < min(len(data['latitude']), len(data['longitude']), len(data['datetime'])):
        query = ("""INSERT INTO geo_analytics (user_id, latitude, longitude, date_time) VALUES (%s, %s, %s, %s)""", (user_id, 
                data['latitude'][i], data['longitude'][i], data['datetime'][i]))
        camp_conn['cursor'].execute("""INSERT INTO geo_analytics (user_id, latitude, longitude, date_time) VALUES (%s, %s, %s, %s)""", (user_id,
                data['latitude'][i], data['longitude'][i], data['datetime'][i]))
        i += 1
    camp_conn['cnx'].commit()
    close_connection(camp_conn['cursor'])
    return


@csrf_exempt
def analytics(request):
#    user_id = str(request.GET['user_id'])
#    time_s = str(request.GET['time_s'])
#    time_f = str(request.GET['time_f'])
    if request.method == 'GET':
        return HttpResponse(loader.get_template('site/analytics.html').render(RequestContext(request,{})))
    if request.method == 'POST':
        camp_conn = open_campaigns_db_connection()
        user_id = str(request.POST['user_id'])
        data = {}
        query_text = "SELECT latitude FROM geo_analytics WHERE user_id=" + user_id + ";"
        data['latitude'] = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
        if not data['latitude']:
            return HttpResponse("No data on this user")
        query_text = "SELECT longitude FROM geo_analytics WHERE user_id=" + user_id + ";"
        data['longitude'] = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
        query_text = "SELECT date_time FROM geo_analytics WHERE user_id=" + user_id + ";"
        data['datetime'] = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
        i = 0
        to_cut_timezone_indices = -6
        while i < len(data['datetime']):
            data['datetime'][i] = data['datetime'][i][:to_cut_timezone_indices]
            data['datetime'][i] = str(datetime.datetime.strptime(data['datetime'][i], "%Y-%m-%dT%H:%M:%S").strftime('%H:%M'))
            i += 1
        result = analyse_geo(data)
#       return HttpResponse(loader.get_template('site/show_analytics.html').render(RequestContext(request, {'centers': data['datetime']})))
        result['geo']['user_id'] = user_id
        original_geo = []
        i = 0
        while i < min(len(data['latitude']), len(data['longitude'])):
            original_geo.append([float(data['latitude'][i]), float(data['longitude'][i])])
            i += 1
        result['geo']['original'] = original_geo
        time_centers = []
        for lab in result['time']:
            time_centers.append(result['time'][lab]['centers'])
        close_connection(camp_conn['cursor'])
        return HttpResponse(loader.get_template('site/show_analytics.html').render(RequestContext(request, {'centers': result['geo']['centers'], 'original': result['geo']['original'], 'user_id': result['geo']['user_id'], 'time_centers': time_centers})))


def analyse_geo(data):
    geo_data = [] # Данные гео
    i = 0
    while i < min(len(data['latitude']), len(data['longitude'])):
        geo_data.append([float(data['latitude'][i]), float(data['longitude'][i])])
        i += 1
    geo_clusters = cluster_geo(geo_data) # Получили номер кластера для каждой точки геолокации
#    return geo_clusters
    original_time = {}
    time_data = {}
    for label in set(geo_clusters['labels']): 
        time_data[label] = []
        original_time[label] = []
    i = 0  
    compare = {} 
    for label in geo_clusters['labels']: # Заполним time_data данными о времени, с которыми KMeans сможет работать
        if label != -1:
            time = data['datetime'][i]
            original_time[label].append(time)
            time = time.split(HourMinuteDelimiter)
            deg = (float(time[0]) * 15.0) + (float(time[1]) / 4.0)
            time_x = np.cos(np.deg2rad(deg))
            time_y = np.sin(np.deg2rad(deg))
            time = [time_x, time_y]
            compare[data['datetime'][i]] = time
            time_data[label].append(time)
        i += 1
    time_clusters = {} 
    for label in set(geo_clusters['labels']): # Тут в time_clusters[label из geo] кладется набор лейблов по времени для времен кластера по geo label
        if label != -1:
            time_clusters[label] = cluster_time(time_data[label], original_time[label])
#           cluster_ind = 0
#           while cluster_ind < len(time_clusters[label]['centers']['labels']):
#               time = time_clusters[label]['centers']['labels'][cluster_ind]
#                minutes = int(floor(to_degrees(acos(float(time[0]))) / 0.25))
#               hours = int(floor(minutes / 60))
#               minutes -= (hours * 60)
#               if hours < 10:
#                   hours = "0" + str(hours)
#               if minutes < 10:
#                   minutes = "0" + str(minutes)
#               time = str(hours) + ":" + str(minutes)
#               time_clusters[label]['centers']['labels'][cluster_ind] = time
#               cluster_ind += 1              
                  
    result = {'geo': geo_clusters, 'time': time_clusters}
    return result 


def analyse_geo_xmeans(data):
    geo_data = [] # Данные гео
    i = 0
    while i < min(len(data['latitude']), len(data['longitude'])):
        geo_data.append([float(data['latitude'][i]), float(data['longitude'][i])])
        i += 1
    geo_clusters = cluster_data(geo_data) # Получили номер кластера для каждой точки геолокации
#    return geo_clusters
    time_data = {}
    for i in range(0, len(geo_clusters['labels'])):
        time_data[i] = []
    
    i = 0
    for label in range(0, len(geo_clusters['labels'])):
        for ind in geo_clusters['labels'][label]: # Заполним time_data данными о времени, с которыми KMeans сможет работать
            time = data['datetime'][ind]
            time = time.split(HourMinuteDelimiter)
            degr = (float(time[0]) * 15.0) + (float(time[1]) / 4.0)
            time_x = np.cos(np.deg2rad(deg))
            time_y = np.sin(np.deg2rad(deg))
            time = [time_x, time_y]
            time_data[label].append(time)
#            i += 1
    time_clusters = {}
    for label in range(0, len(geo_clusters['labels'])): # Тут в time_clusters[label из geo] кладется набор лейблов по времени для времен кластера по geo label
        time_clusters[label] = cluster_data(time_data[label])
    final_geo_labels = [None]*len(min(data['latitude'], data['longitude']))
    for label in range(len(geo_clusters['labels'])):
        for ind in geo_clusters['labels'][label]:
            final_geo_labels[ind] = label
    geo_clusters['labels'] = final_geo_labels
    result = {'geo': geo_clusters, 'time': time_clusters}
    return result


def get_favourite_item(user_id, order_num):
    connection = open_main_db_connection()
    query_text = "SELECT id FROM orders where user_id=" + "\'" + user_id + "\'" + ";"
    data = make_query(query_text, connection['cursor'], connection['cnx'])
    all_item_id = []
    for order_id in data:
        query_text = "SELECT position_id FROM order_menu_positions where order_id=" + "\'" + str(order_id) + "\'" + ";"
        position_ids = make_query(query_text, connection['cursor'], connection['cnx'])
        for position_id in position_ids:
            query_text = "SELECT menu_item_id FROM menu_positions where id=" + "\'" + str(position_id) + "\'" + ";"
            all_item_id += make_query(query_text, connection['cursor'], connection['cnx'])
    all_item_id_dict = {}
    all_item_id_set = set(all_item_id)
    for item_id in all_item_id_set:
        all_item_id_dict[item_id] = all_item_id.count(item_id)
    sorted_item_id = sorted([(value,key) for (key,value) in all_item_id_dict.items()], reverse=True)
    close_connection(connection['cursor'])
    return sorted_item_id[order_num - 1][1]     


def recommend(request):
    connection = open_main_db_connection()
    if request.method == 'GET' and 'user_id' not in request.GET:
        query_text = "SELECT user_id FROM orders;"
        users = make_query(query_text, connection['cursor'], connection['cnx'])
        users = list(set(users))
#        users = []
#        for user_id in data:
#            query_text = "SELECT venue_id FROM orders where user_id=" + "\'" + str(user_id) + "\'" + ";"
#            user_data = make_query(query_text, connection['cursor'], connection['cnx'])
#            if len(user_data):
#                users.append(str(int(user_id)))
        #users = json.dumps(users)
        close_connection(connection['cursor'])
        return HttpResponse(loader.get_template('site/recommend.html').render(RequestContext(request,{'users': users})))
    elif 'user_id' in request.GET:
        user_id = request.GET['user_id']
        query_text = "SELECT venue_id FROM orders where user_id=" + "\'" + str(user_id) + "\'" + ";"
        data = make_query(query_text, connection['cursor'], connection['cnx'])
        if len(data):
            #if request.GET['method'] == "Venue number CF":
	    #    recommend_venues_vncf = cf_venue_number_recommend(user_id) # Collaborative filtering.
            #elif request.GET['method'] == "Tag number CF":
            #    recommend_venues_tncf = cf_tag_number_recommend(user_id)
#            elif request.GET['method'] == "Weighted tag number CF":
#                recommend_venues = cf_weighted_tag_number_recommend(user_id)
            #elif request.GET['method'] == "Item-based":
            #    recommend_venues_ib = item_based_recommend(user_id)
            recommend_venues_vncf = cf_venue_number_recommend(user_id) # Collaborative filtering.
            venues_vncf = []
            for venue_id in recommend_venues_vncf:
                query_text = "SELECT name FROM venues where id=" + "\'" + str(venue_id) + "\'" + ";"
                to_add = make_query(query_text, connection['cursor'], connection['cnx'])
                if to_add:
                    venues_vncf.append(to_add[0])
            venues_vncf = json.dumps(venues_vncf)
            
            recommend_venues_tncf = cf_tag_number_recommend(user_id)
            venues_tncf = []
            for venue_id in recommend_venues_tncf:
                query_text = "SELECT name FROM venues where id=" + "\'" + str(venue_id) + "\'" + ";"
                to_add = make_query(query_text, connection['cursor'], connection['cnx'])
                if to_add:
                    venues_tncf.append(to_add[0])
            venues_tncf = json.dumps(venues_tncf)

            recommend_venues_ib = item_based_recommend(user_id)
            venues_ib = []
            for venue_id in recommend_venues_ib:
                query_text = "SELECT name FROM venues where id=" + "\'" + str(venue_id) + "\'" + ";"
                to_add = make_query(query_text, connection['cursor'], connection['cnx'])
                if to_add:
                    venues_ib.append(to_add[0])
            venues_ib = json.dumps(venues_ib)
            
            close_connection(connection['cursor'])
            return HttpResponse(loader.get_template('site/show_recommend.html').render(RequestContext(request, {'method': ["Venue number SVD", "Venue number SVD", "Item-based"], 'venues_vncf': venues_vncf, 'venues_tncf': venues_tncf, 'venues_ib': venues_ib, 'user_id': user_id})))
        else:
            close_connection(connection['cursor'])
            return HttpResponse("No data on this user")
    else:
        close_connection(connection['cursor'])
        return HttpResponse(loader.get_template('site/recommend.html').render(RequestContext(request,{})))


def recommend_items_to_order(request):
    items = request.GET['items']
    result = get_items_apriori(items)
    connection = open_main_db_connection()
    id_list = ""
    for item_id in result:
        id_list += '\'' + str(item_id) + '\'' + ', '
    id_list = id_list[:-2]
    query_text = "SELECT name FROM menu_items where id IN (" + id_list + ");"
    item_names = make_query(query_text, connection['cursor'], connection['cnx'])
#    item_names = str(item_names)[1:-1]
    to_return = ""
    for item in item_names:
        to_return += item + ", "
    to_return = to_return[:-2] 
    return HttpResponse(content=to_return, status=200)


@csrf_exempt
def campaign_menu(request):
    if 'create' in request.POST:
        create(request)
    camp_conn = open_campaigns_db_connection()
    query_text = "SELECT * FROM campaigns;"
    camp_data = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
    camp_data = json.dumps(camp_data)
    close_connection(camp_conn['cursor'])
    connection = open_main_db_connection()

    query_text = "SELECT id, name FROM venues;"
    id_name_venues = make_query(query_text, connection['cursor'], connection['cnx'])

    query_text = "SELECT DISTINCT id, name, venue_id FROM menu_items;"
    id_name_venue_id_item = make_query(query_text, connection['cursor'], connection['cnx'])

    id_to_name_venues = {}

    venue_to_items = {}
    index = 0
    while index < len(id_name_venues):
        venue_id = str(id_name_venues[index])
        venue_name = id_name_venues[index + 1]
        id_to_name_venues[venue_id] = venue_name
        venue_to_items[venue_id + " " + venue_name] = []
        index += 2

    index = 0
    while index < len(id_name_venue_id_item):
        item_id = str(id_name_venue_id_item[index])
        item_name = id_name_venue_id_item[index + 1]
        venue_id = str(id_name_venue_id_item[index + 2])
        venue_to_items[venue_id + " " + id_to_name_venues[venue_id]].append(item_id + " " + item_name)
        index += 3
    close_connection(connection['cursor'])
    return HttpResponse(loader.get_template('site/campaign_menu.html').render(RequestContext(request, {'data': camp_data, 'venue_item': json.dumps(venue_to_items)})))


def event_log(request):
    camp_conn = open_campaigns_db_connection()
    query_text = "SELECT * FROM all_push;"
    data = make_query(query_text, camp_conn['cursor'], camp_conn['cnx'])
    close_connection(camp_conn['cursor'])
    return HttpResponse(loader.get_template('site/event_log.html').render(RequestContext(request, {'data': json.dumps(data)})))


def get_distance_meters(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    distance_meters = 6367 * 2 * asin(sqrt(sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2)) * 1000
    return distance_meters

